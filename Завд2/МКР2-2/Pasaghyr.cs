﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace МКР2_2
{
    [Serializable]
    public partial class Pasaghyr
    {
        public string Surname { get; set; }
        public string Destination { get; set; }
        public int CountOfLaugage { get; set; }
        public int Weight { get; set; }
        public Pasaghyr(string _surname, string _destination, int _count, int _weight)
        {
            Surname = _surname;
            Destination = _destination;
            CountOfLaugage = _count;
            Weight = _weight;

        }

    }
}
