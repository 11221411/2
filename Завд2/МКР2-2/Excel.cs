﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelApp = Microsoft.Office.Interop.Excel;
namespace МКР2_2
{
    internal class Excel
    {
        public static void saveGrid(DataGridView grid, string fileName, string sheetName)
        {
            ExcelApp.Application excelApp = new Microsoft.Office.Interop.Excel.Application();

            excelApp.Visible = true;

            excelApp.SheetsInNewWorkbook = 1;

            ExcelApp.Workbook workBook = excelApp.Workbooks.Add(Type.Missing);

            excelApp.DisplayAlerts = false;

            ExcelApp.Worksheet sheet = (ExcelApp.Worksheet)excelApp.Worksheets.get_Item(1);

            sheet.Name = sheetName;

            int offsetRow = 1;

            for (int j = 0; j < grid.ColumnCount; j++)
            {
                sheet.Cells[offsetRow, j + 1] = grid.Columns[j].HeaderText;
            }

            for (int i = 0; i < grid.RowCount; i++)
            {
                for (int j = 0; j < grid.ColumnCount; j++)
                    sheet.Cells[offsetRow + i + 1, j + 1] = grid[j, i].Value;
            }

            ExcelApp.Range range1 = sheet.Range[sheet.Cells[offsetRow, 1], sheet.Cells[offsetRow, grid.ColumnCount]];

            range1.Cells.Font.Name = "Colibri";

            range1.Cells.Font.Size = 12;

            range1.Cells.Font.Color = ColorTranslator.ToOle(Color.White);

            range1.Interior.Color = ColorTranslator.ToOle(Color.Blue);

            excelApp.Application.ActiveWorkbook.SaveAs($"{fileName}.xlsx");

            workBook.Close(true);

            excelApp.Quit();
        }
    }
}
