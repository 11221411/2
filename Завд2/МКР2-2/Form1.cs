﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelApp = Microsoft.Office.Interop.Excel;

namespace МКР2_2
{
    public partial class Pasaghyr : Form
    {
        List<Pasaghyr> PasaghyrList;
        BindingSource BindSource;
        public Pasaghyr()
        {
            InitializeComponent();
        }

        private void Pasaghyr_Load(object sender, EventArgs e)
        {
            PasaghyrList = new List<Pasaghyr>();

            PasaghyrList.Add(new Pasaghyr("Новікова", "Італія", 5, 50));
            PasaghyrList.Add(new Pasaghyr("Іванов", "Греція", 2, 30));
            PasaghyrList.Add(new Pasaghyr("Ганіч", "Греція", 6, 70));
            PasaghyrList.Add(new Pasaghyr("Сидоренко", "Англія", 4, 50));
            PasaghyrList.Add(new Pasaghyr("Кудланова", "Греція", 8, 90));


            BindSource = new BindingSource();

            BindSource.DataSource = PasaghyrList;

            dataGridView1.DataSource = BindSource;

            dataGridView1.Columns["Surname"].HeaderText = "Прізвище";
            dataGridView1.Columns["Destination"].HeaderText = "Пункт призначення";
            dataGridView1.Columns["Count of laugage"].HeaderText = "Кількість багажу";
            dataGridView1.Columns["Weight"].HeaderText = "Маса багажу";


        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, PasaghyrList);
                fs.Close();
            }
        }

        private void імпортToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryFormatter bf = new BinaryFormatter();
                BindSource.Clear();
                foreach (Pasaghyr b in (List<Pasaghyr>)bf.Deserialize(fs))
                    BindSource.Add(b);
                fs.Close();
            }
        }
        List<Pasaghyr> PasaghyrListFiltered;

        BindingSource BindSourceFiltered;

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void експортToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                Excel.saveGrid(dataGridView1, saveFileDialog.FileName, "Export_data");
            }
        }

        private void вивестиЗагальнуКількістьМісцьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(CountOfLaugage.ToString());
        }

        private void визначитиСумуМасиБагажуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string country = "Греція";
            double sum = 0;
            double temp = 0;
            PasaghyrListFiltered = PasaghyrList.FindAll(Pasaghyr => Pasaghyr.Destination == country);
            foreach (var item in PasaghyrListFiltered)
            {
                temp += item.Weight;
            }
            sum += temp;
            MessageBox.Show("Sum " + sum.ToString());
        }
    }
}
