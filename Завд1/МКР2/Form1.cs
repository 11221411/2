﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace МКР2
{
    public partial class Form1 : Form
    {
        int[] mas;
        int n;
        Random ran = new Random();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.dataGridView1.RowCount = 1;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            n=(int)this.numericUpDown1.Value;
            this.dataGridView1.ColumnCount= n;
            mas = new int[n];
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = ran.Next(1, 100);
                this.dataGridView1.Rows[0].Cells[i].Value = mas[i];
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double avarage = mas.Average();
            textBox1.Text = " " +avarage.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double product = 1;
            for (int i = 0; i < mas.Length; i++)
            {
                product *= mas[i];
            }
            double geometricMean = Math.Pow(product, 1.0/mas.Length);
            textBox2.Text = " " + geometricMean.ToString();
        }
    }
}
