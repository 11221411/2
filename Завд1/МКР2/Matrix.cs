﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace МКР2
{
    internal class Matrix
    {
        double[] n;
        int columnCount;
        public Matrix(int lenght, int min = 1, int max = 100)
        {
            n = getRandomMatrix(lenght, min, max);
        }
        public Matrix(DataGridView grid)
        {
            n = readMatrixFromGrid(grid);
        }
        public Matrix(int columnCount)
        {
            this.columnCount=columnCount;
        }
        double[] getRandomMatrix(int lenght, int min = 1, int max = 100)
        {
            double[] n = new double[lenght];
            Random random= new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < lenght; i++)
            {
                n[i] = random.Next(min, max);
            }
            return n;
        }
        public static double[] readMatrixFromGrid(DataGridView grid)
        {
            double[] n = new double[grid.ColumnCount];
            for (int i = 0; i < grid.ColumnCount;i++)
            {
                n[i] = Convert.ToDouble(grid[i, 0]);
            }
            return n;
        }
        public void writeToGrid(DataGridView grid)
        {
            grid.ColumnCount = n.Length;
            for (int j = 0; j < n.Length; j++)
            {
                grid[j, 0].Value = n[j];
            }
        }
    }

}
